package edu.uchicago.gerber.labjetnavigation;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;

import androidx.navigation.Navigation;
import edu.uchicago.gerber.labjetnavigation.dummy.DummyContent;


public class MainActivity extends AppCompatActivity  implements ListFragment.OnListFragmentInteractionListener{
    //private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    Navigation.findNavController(MainActivity.this,R.id.nav_host ).navigate(R.id.mainFragment);
                    return true;
                case R.id.navigation_dashboard:
                    Navigation.findNavController(MainActivity.this,R.id.nav_host ).navigate(R.id.listFragment);
                    return true;
                case R.id.navigation_notifications:
                    //Bundle bundle = new Bundle();
                   // bundle.putString(DataFragment.ARG_PARAM1, "Some Data");
                   // Navigation.findNavController(MainActivity.this,R.id.nav_host ).navigate(R.id.dataFragment, bundle);
                    return true;
            }
            return false;
        }
    };
    private BottomNavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navView = findViewById(R.id.nav_view);
      //  mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

        Log.d("Navigation", "You selected " + item);
        navView.setSelectedItemId(R.id.navigation_notifications);
        Bundle bundle = new Bundle();
        bundle.putString(DataFragment.ARG_PARAM1, item.toString());
        Navigation.findNavController(MainActivity.this,R.id.nav_host ).navigate(R.id.action_listFragment_to_dataFragment, bundle);



    }

    //every navigation step is automatically registered on the backstack

}
